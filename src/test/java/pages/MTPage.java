package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class MTPage extends BasePage {

    @FindBy(xpath = "//input[@name='username']")
    private WebElement userLoginElement;

    @FindBy(xpath = "//input[@name='password']")
    private WebElement userPasswordElement;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement logInButton;

    @FindBy(xpath = "//input[@id='search']")
    private WebElement searchInput;

    public MTPage(RemoteWebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void enterLogin(String login) {
        sendKeysToElement(userLoginElement, login);
    }

    public void enterPassword(String password) {
        sendKeysToElement(userPasswordElement, password);
    }

    public void logInClick() {
        clickElement(logInButton);
    }

    public boolean isLogIn() {
        return new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(searchInput)).isDisplayed();
    }
}
