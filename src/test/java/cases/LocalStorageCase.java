package cases;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.core.io.ClassPathResource;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.MTPage;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class LocalStorageCase {

    private final String MT_PAGE_URL = "https://maketalents.simbirsoft1.com/auth/login/";
    private String userLogin = "";
    private String userPassword = "";
    private RemoteWebDriver driver;
    private MTPage mtPage;
    private boolean isUserInfoExist;

    @BeforeClass
    public void setUp() throws Exception {
        ChromeDriverManager.getInstance().version("2.29").setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        initProperties();
    }

    @Test
    public void testLocalStorage() throws IOException {
        driver.get(MT_PAGE_URL);
        mtPage = new MTPage(driver);
        logIn();
        printUserInfo();
        Assert.assertTrue(isUserInfoExist);
    }

    private void logIn() {
        mtPage.enterLogin(userLogin);
        mtPage.enterPassword(userPassword);
        mtPage.logInClick();
    }

    private void printUserInfo() {
        if (mtPage.isLogIn()) {
            JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
            String storageValue = (String) jsExecutor.executeScript("return localStorage.getItem('ngStorage-user')");

            JsonObject user = new JsonParser().parse(storageValue).getAsJsonObject();
            JsonObject employee = user.getAsJsonObject("employee");
            isUserInfoExist = employee.size() > 0;
            JsonArray roles = user.getAsJsonArray("roles");
            System.out.println("Last name - " + employee.get("lastName"));
            System.out.println("First name - " + employee.get("firstName"));
            System.out.println("skype - " + employee.get("skype"));
            System.out.println("phone - " + employee.get("phone"));
            JsonArray levels = employee.getAsJsonArray("englishLevel");
            for (JsonElement level : levels) {
                System.out.println("englishLevel - " + level.toString());
            }

            for (JsonElement role : roles) {
                System.out.println("Role - " + role.getAsJsonObject().get("name"));
            }
        }
    }

    private void initProperties() throws IOException {
        Properties config = new Properties();
        try {
            config.load(new ClassPathResource("config.properties").getInputStream());
            userLogin = config.getProperty("mt.user.login");
            userPassword = config.getProperty("mt.user.password");
        } catch (IOException ex) {
            ex.printStackTrace();
            fail("File 'config.properties' can't be open!");
        }
    }

    @AfterClass
    public void tearDown() throws Exception {
        driver.close();
        driver.quit();
    }
}
