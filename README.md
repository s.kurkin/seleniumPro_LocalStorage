Задание:
1) С помощью Selenium открыть сайт maketalents.simbirsoft1.com
2) Авторизоваться
3) Считать из LocalStorage значение переменной ngStorage-user
4) Получить из считанного массива и вывести в консоль свои имя, фамилию, скайп, номер телефона, роли, уровень английского
     
Запуск теста:

1) В файле \src\test\resources\config.properties заполнить проперти соответствующими значениями:  
mt.user.login - логин пользователя
mt.user.password - пароль пользователя 	 

1) mvn clean -Dtest=LocalStorageCase test